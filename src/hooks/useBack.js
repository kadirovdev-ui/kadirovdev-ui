import {useRouter} from "vue-router";

export default function () {
    const router = useRouter()

    const back = () => router.back()

    return {
        back
    }
}