import {createRouter, createWebHistory} from 'vue-router'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('@/pages/HomePage.vue')
        },
        {
            path: '/about-course',
            component: () => import('@/pages/AboutCoursePage.vue')
        },
        {
            path: '/sign-in',
            component: () => import('@/pages/SignInPage.vue')
        },
        {
            path: '/sign-up',
            component: () => import('@/pages/SignUpPage.vue')
        },
        {
            path: '/new-password',
            component: () => import('@/pages/CreateNewPasswordPage.vue')
        },
        {
            path: '/recover-password',
            component: () => import('@/pages/RecoverPasswordPage.vue')
        },
        {
            path: '/confirm-by-code',
            component: () => import('@/pages/ConfirmEmailForRecoverPasswordPage.vue')
        },
        {
            path: '/profile',
            name: 'profile',
            component: () => import('@/pages/ProfilePage.vue'),
            children: [
                {
                    path: '/profile/',
                    name: 'comment',
                    component: () => import('@/components/CommentComponent.vue')
                },
                {
                    path: '/profile/coins',
                    name: 'coins',
                    component: () => import('@/components/MyCoinsComponent.vue')
                },
                {
                    path: '/profile/settings',
                    name: 'settings',
                    component: () => import('@/components/SettingsComponent.vue')
                },
                {
                    path: '/profile/my-courses/',
                    name: 'my-courses',
                    component: () => import('@/components/MyCoursesComponent.vue')
                },
                {
                    path: '/profile/my-courses/course',
                    name: 'my-course',
                    component: () => import('@/components/MyCourseComponent.vue'),
                },
                {
                    path: '/profile/another-flow',
                    name: 'another-flows',
                    component: () => import('@/components/AnotherFlowComponent.vue')
                },
                {
                    path: '/profile/another-flow/info-course',
                    name: 'another-flow',
                    component: () => import('@/components/AnotherFlowCourseInfoComponent.vue')
                }
            ]
        },
    ],
    linkActiveClass: 'headerActiveLink'
})

export default router
