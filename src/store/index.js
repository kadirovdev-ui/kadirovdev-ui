import {createStore} from "vuex";
import popup from "@/store/popup.js";

export default createStore({
    modules: {
        popup
    }
})