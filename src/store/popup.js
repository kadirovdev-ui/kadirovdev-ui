export default {
    mutations: {
        updatePopup(state, data) {
            state.name = data.name
            state.status = data.value
        }
    },
    state: {
        name: '',
        status: false
    },
    getters: {
        getPopupName(state) {
            return state.name
        },
        getPopupStatus(state) {
            return state.status
        }
    }
}