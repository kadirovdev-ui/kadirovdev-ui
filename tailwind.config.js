/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    screens: {
      'xs': '400px',
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    container: {
      center: true,
      padding: {
        DEFAULT: '1.25rem',
        sm: '1.5rem',
        lg: '2rem',
        xl: '2.5rem',
        '2xl': '3rem'
      },
    },
    colors: {
      'transparent': '#00000000',
      'black': '#000000',
      'black-lighter': '#1A1A1A',
      'white': '#FFFFFF',
      'auth_bg': '#EEF2F5',
      'main_color': '#80007F',
      'main_bg': '#F5F3F2',
      'gray_blue': '#A3B2BD',
      'gray_bg': '#323232',
      'gray_hover': 'rgb(115,115,115)',
      'gray_border': '#b6b6b6',
      'gray_outline': '#cccccc',
      'gray_hr': '#DFDFDF',
      'gray_bottom_tab': '#d5d5d5',
      'no_color': 'rgba(221,221,221,0)',
      'red_validation': '#ED2709',
      'blue': '#3EA2FF',
      'purple_link': '#BF8DFF',
      'purple_arrows_bg': '#dae0ff',
      'purple_tab': 'rgba(128,0,127,0.55)',
      'yellow': '#FFD335',
      'lime-green': '#00AF3C'
    },
    fontFamily: {
      gilroy: ['Gilroy', 'sans-serif'],
      sans: ['Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif']
    },
    fontSize: {
      'm-sm': ['1rem', {
        lineHeight: '1.19625rem',
        fontWeight: '500',
      }],
      'm-lg': ['1.375rem', {
        lineHeight: '1.668125rem',
        fontWeight: '500',
      }],
      sm: '0.75rem',
      md: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '3.75rem',
      '7xl': '4.5rem',
      '8xl': '6rem',
      '9xl': '8rem',
    }
  },
  plugins: [
  ],
}

